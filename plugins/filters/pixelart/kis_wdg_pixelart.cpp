/*
 * This file is part of Krita
 *
 * Copyright (c) 2006 Cyrille Berger <cberger@cberger.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "kis_wdg_pixelart.h"
#include <QLayout>
#include <QToolButton>
#include <QIcon>
#include <QFileDialog>
#include <QMessageBox>

#include <filter/kis_filter.h>
#include <filter/kis_filter_configuration.h>
#include <kis_selection.h>
#include <kis_paint_device.h>
#include <kis_processing_information.h>

#include "ui_wdgpixelart.h"

KisWdgPixelArt::KisWdgPixelArt(QWidget * parent) : KisConfigWidget(parent)
{
    m_widget = new Ui_WdgPixelArt();
    m_widget->setupUi(this);
    linkSpacingToggled(true);

    connect(widget()->aspectButton, SIGNAL(keepAspectRatioChanged(bool)), this, SLOT(linkSpacingToggled(bool)));
    connect(widget()->pixelWidth, SIGNAL(valueChanged(int)), this, SLOT(spinBoxHalfWidthChanged(int)));
    connect(widget()->pixelHeight, SIGNAL(valueChanged(int)), this, SLOT(spinBoxHalfHeightChanged(int)));

    connect(widget()->paletteLoad, SIGNAL(pressed()), SLOT(pickPaletteFile()));
    connect(widget()->algorithm, SIGNAL(currentIndexChanged(int)), SIGNAL(sigConfigurationItemChanged()));
    connect(widget()->useLab, SIGNAL(currentIndexChanged(int)), SIGNAL(sigConfigurationItemChanged()));
}

KisWdgPixelArt::~KisWdgPixelArt()
{
    delete m_widget;
}

KisPropertiesConfigurationSP KisWdgPixelArt::configuration() const
{
    KisFilterConfigurationSP config = new KisFilterConfiguration("pixelArt", 1);
    config->setProperty("lockAspect", widget()->aspectButton->keepAspectRatio());
    config->setProperty("pixelWidth", widget()->pixelWidth->value());
    config->setProperty("pixelHeight", widget()->pixelHeight->value());
    config->setProperty("algorithm", widget()->algorithm->currentIndex());
    config->setProperty("useLab", widget()->useLab->currentIndex());

    QStringList palette;
    for(auto && color : m_colors) {
        palette.push_back(QString::number(color));
    }

    config->setProperty("palette", palette);
    return config;
}

void KisWdgPixelArt::setConfiguration(const KisPropertiesConfigurationSP config)
{
    QVariant value;
    if (config->getProperty("lockAspect", value)) {
        m_widget->aspectButton->setKeepAspectRatio(value.toBool());
    }
    if (config->getProperty("algorithm", value)) {
        widget()->algorithm->setCurrentIndex(value.toUInt());
    }
    if (config->getProperty("useLab", value)) {
        widget()->useLab->setCurrentIndex(value.toUInt());
    }
    if (config->getProperty("pixelWidth", value)) {
        widget()->pixelWidth->setValue(value.toUInt());
    }
    if (config->getProperty("pixelHeight", value)) {
        widget()->pixelHeight->setValue(value.toUInt());
    }

    QStringList palette = config->getStringList("palette", palette);
    for(auto && color : palette) {
        m_colors.push_back(color.toInt());
    }

    widget()->paletteColors->setText(QString::number(m_colors.size()));
}

void KisWdgPixelArt::linkSpacingToggled(bool b)
{
    m_sizeLink = b;
    widget()->pixelHeight->setValue(widget()->pixelWidth->value());
}

void KisWdgPixelArt::pickPaletteFile()
{
    auto file_name = QFileDialog::getOpenFileName(this,
        tr("Open Image"), QDir::currentPath(), tr("Image Files (*.png *.jpg *.bmp)"));

    if(file_name.size() == 0) {
        return;
    }

    auto load_file_fail = []()
    {
      QMessageBox message_box;
      message_box.critical(0, QString("Error creating palette"), QString("Could not load image file"));
      message_box.setFixedSize(500,200);
    };

    try
    {
      QImage image(file_name);

      if(image.isNull())
      {
        return;
      }

      QSet<QRgb> palette;
      for(int y = 0; y < image.height(); ++y)
      {
        for(int x = 0; x < image.width(); ++x)
        {
          auto color = image.pixel(x, y);
          palette.insert(color);
        }
      }

      m_colors.clear();
      for(auto & color : palette)
      {
        m_colors.push_back(color);
      }
    }
    catch(...)
    {
      load_file_fail();
      return;
    }

    widget()->paletteColors->setText(QString::number(m_colors.size()));

    emit sigConfigurationItemChanged();
}

void KisWdgPixelArt::spinBoxHalfWidthChanged(int v)
{
    if (m_sizeLink) {
        widget()->pixelHeight->setValue(v);
    }
    /*    if( widget()->intHalfHeight->value() == v && widget()->cbShape->currentItem() != 1)
            widget()->intAngle->setEnabled(false);
        else
            widget()->intAngle->setEnabled(true);*/
    emit sigConfigurationItemChanged();
}

void KisWdgPixelArt::spinBoxHalfHeightChanged(int v)
{
    if (m_sizeLink) {
        widget()->pixelWidth->setValue(v);
    }
    /*    if( widget()->intHalfWidth->value() == v && widget()->cbShape->currentItem() != 1)
            widget()->intAngle->setEnabled(false);
        else
            widget()->intAngle->setEnabled(true);*/
    emit sigConfigurationItemChanged();
}

