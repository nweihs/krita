/*
 * This file is part of the KDE project
 *
 * Copyright (c) Michael Thaler <michael.thaler@physik.tu-muenchen.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _KIS_PIXELIZE_FILTER_H_
#define _KIS_PIXELIZE_FILTER_H_

#include "filter/kis_filter.h"
#include "kis_config_widget.h"

enum class PixelArtDitherAlgorithm
{
    None,
    Bayer2x2,
    Bayer4x4,
    Bayer8x8,
    FloydSteinberg,
    JarvisJudiceNinke,
    Stucki,
    Atkinson,
    Burkes,
    Sierra,
    SierraTwoRow,
    SierraLite,
};

struct PixelArtLab
{
    quint16 laba[4];
};

struct PixelArtRGBA
{
    quint16 rgba[4];
};

struct PixelArtRGBAError
{
    qint32 rgba[4];
};

struct PixelArtPaletteEntry
{
    PixelArtLab lab;
    PixelArtRGBA rgba;
};

struct PixelArtBayerKernel
{
    QVector<int> matrix;
    qint32 size;
};

struct PixelArtErrorDiffusionKernelElement
{
    qint32 x, y;
    qint32 scale;
};

struct PixelArtErrorDiffusionKernel
{
    QVector<PixelArtErrorDiffusionKernelElement> kernelElements;
    qint32 denom;
};

class KisPixelArtFilter :
    public KisFilter
{
public:
    KisPixelArtFilter();

public:

    void processImpl(KisPaintDeviceSP device,
        const QRect & applyRect,
        const KisFilterConfigurationSP config,
        KoUpdater * progressUpdater) const override;

    static inline KoID id()
    {
        return KoID("pixelArt", i18n("Pixel Art"));
    }

    QRect neededRect(const QRect & rect, const KisFilterConfigurationSP config, int lod) const override;

    QRect changedRect(const QRect & rect, const KisFilterConfigurationSP config, int lod) const override;

public:
    KisConfigWidget *
    createConfigurationWidget(QWidget * parent, const KisPaintDeviceSP dev, bool useForMasks) const override;

    KisFilterConfigurationSP factoryConfiguration() const override;

private:

    struct PaletteFindResult
    {
        PixelArtRGBA color;
        PixelArtRGBAError error;
    };

    struct PaletteFindTwoClosestResult
    {
        PixelArtRGBA color1;
        PixelArtLab lab1;
        quint64 colorDiff1;

        PixelArtRGBA color2;
        PixelArtLab lab2;
        quint64 colorDiff2;
    };

    static quint64 diffColor(PixelArtLab lab1, PixelArtLab lab2);
    static quint64 diffColor(PixelArtRGBA lab1, PixelArtRGBA lab2);


    static void scaleAppendErrorShift(PixelArtRGBAError & error, const PixelArtRGBAError & append, quint32 scale, quint32 shift);
    static void scaleAppendErrorDivide(PixelArtRGBAError & error, const PixelArtRGBAError & append, quint32 scale, quint32 denom);
    static bool isErrorDiffusionAlgorithm(PixelArtDitherAlgorithm algorithm);

    static PaletteFindResult findClosestColor(
        const PixelArtRGBA & color,
        bool useLab,
        const KoColorSpace * rgbColorSpace,
        const QVector<PixelArtPaletteEntry> & palette);

    static PaletteFindTwoClosestResult findTwoClosestColors(
        const PixelArtRGBA & color,
        bool useLab,
        const KoColorSpace * rgbColorSpace,
        const QVector<PixelArtPaletteEntry> & palette);

    void processPixelsNoDither(
        KisPaintDeviceSP device,
        const QRect & applyRect,
        const KisFilterConfigurationSP config,
        KoUpdater * progressUpdater,
        const QVector<PixelArtPaletteEntry> & palette) const;

    void processPixelsBayer(
        KisPaintDeviceSP device,
        const QRect & applyRect,
        const KisFilterConfigurationSP config,
        KoUpdater * progressUpdater,
        const QVector<PixelArtPaletteEntry> & palette,
        const PixelArtBayerKernel & kernel) const;

    void processPixelsErrorDiffusion(
        KisPaintDeviceSP device,
        const QRect & applyRect,
        const KisFilterConfigurationSP config,
        KoUpdater * progressUpdater,
        const QVector<PixelArtPaletteEntry> & palette,
        const PixelArtErrorDiffusionKernel & kernel) const;

};

#endif
