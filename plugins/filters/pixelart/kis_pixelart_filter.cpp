/*
 * This file is part of Krita
 *
 * Copyright (c) 2005 Michael Thaler <michael.thaler@physik.tu-muenchen.de>
 *
 * ported from Gimp, Copyright (C) 1997 Eiichi Takamori <taka@ma1.seikyou.ne.jp>
 * original pixelArt.c for GIMP 0.54 by Tracy Scott
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "kis_pixelart_filter.h"


#include <stdlib.h>
#include <float.h>
#include <vector>

#include <QPoint>
#include <QSpinBox>
#include <QVector>

#include <klocalizedstring.h>
#include <kpluginfactory.h>

#include <KoUpdater.h>

#include <kis_debug.h>
#include <KisDocument.h>
#include <filter/kis_filter_registry.h>
#include <kis_global.h>
#include <kis_image.h>
#include <kis_layer.h>
#include <kis_selection.h>
#include <kis_types.h>
#include <filter/kis_filter_category_ids.h>
#include <filter/kis_filter_configuration.h>
#include <kis_processing_information.h>


#include "widgets/kis_multi_integer_filter_widget.h"
#include <KoMixColorsOp.h>
#include <KisSequentialIteratorProgress.h>
#include "kis_algebra_2d.h"
#include "kis_lod_transform.h"
#include "kis_wdg_pixelart.h"
#include "ui_wdgpixelart.h"

KisPixelArtFilter::KisPixelArtFilter() : KisFilter(id(), FiltersCategoryArtisticId, i18n("&Pixel Art..."))
{
    setSupportsPainting(true);
    setSupportsThreading(false);
    setSupportsAdjustmentLayers(true);
    setColorSpaceIndependence(FULLY_INDEPENDENT);
}

void KisPixelArtFilter::processImpl(
    KisPaintDeviceSP device,
    const QRect & applyRect,
    const KisFilterConfigurationSP config,
    KoUpdater * progressUpdater) const
{
    Q_ASSERT(device);

    const KoColorSpace * cs = device->colorSpace();

    QVector<PixelArtPaletteEntry> palette;
    QStringList paletteStrings = config->getStringList("palette");

    for (QString & colorStr : paletteStrings) {
        QRgb rgb = static_cast<QRgb>(colorStr.toInt());
        KoColor color(QColor(rgb), cs);

        PixelArtRGBA rgba;
        cs->toRgbA16(reinterpret_cast<const quint8 *>(color.data()), reinterpret_cast<quint8 *>(&rgba), 1);

        PixelArtLab lab;
        cs->toLabA16(reinterpret_cast<const quint8 *>(color.data()), reinterpret_cast<quint8 *>(&lab), 1);
        palette.push_back(PixelArtPaletteEntry{ lab, rgba });
    }

    PixelArtBayerKernel bayer2Kernel = {
        {
            0,  2,
            3,  1
        },
        2
    };

    PixelArtBayerKernel bayer4Kernel = {
        {
            0,  8,  2,  10,
            12, 4,  14, 6,
            3,  11, 1,  9,
            15, 7,  13, 5
        },
        4
    };

    PixelArtBayerKernel bayer8Kernel = {
        {
            0,  48, 12, 60, 3,  51, 15, 63,
            32, 16, 44, 28, 35, 19, 47, 31,
            8,  56, 4,  52, 11, 59, 7,  55,
            40, 24, 36, 20, 43, 27, 39, 23,
            2,  50, 14, 62, 1,  49, 13, 61,
            34, 18, 46, 30, 33, 17, 45, 29,
            10, 58, 6,  54, 9,  57, 5,  53,
            42, 26, 38, 22, 41, 25, 37, 21
        },
        8
    };

    // https://en.wikipedia.org/wiki/Floyd–Steinberg_dithering
    PixelArtErrorDiffusionKernel floydSteinbergKernel = {
        {
            { 1, 0, 7 },
            { -1, 1, 3 },
            { 0, 1, 5 },
            { 1, 1, 1 }
        },
        16
    };

    PixelArtErrorDiffusionKernel jarvisJudiceNinkeKernel = {
        {
            { 1, 0, 7 },
            { 2, 0, 5 },
            { -2, 1, 3 },
            { -1, 1, 5 },
            { 0, 1, 7 },
            { 1, 1, 5 },
            { 2, 1, 3 },
            { -2, 2, 1 },
            { -1, 2, 3 },
            { 0, 2, 5 },
            { 1, 2, 3 },
            { 2, 2, 1 },
        },
        48
    };

    PixelArtErrorDiffusionKernel stuckiKernel = {
        {
            { 1, 0, 8 },
            { 2, 0, 4 },
            { -2, 1, 2 },
            { -1, 1, 4 },
            { 0, 1, 8 },
            { 1, 1, 4 },
            { 2, 1, 2 },
            { -2, 2, 1 },
            { -1, 2, 2 },
            { 0, 2, 4 },
            { 1, 2, 2 },
            { 2, 2, 1 },
        },
        42
    };

    PixelArtErrorDiffusionKernel atkinsonKernel = {
        {
            { 1, 0, 1 },
            { 2, 0, 1 },
            { -1, 1, 1 },
            { 0, 1, 1 },
            { 1, 1, 1 },
            { 0, 2, 1 }
        },
        8
    };

    PixelArtErrorDiffusionKernel burkesKernel = {
        {
            { 1, 0, 8 },
            { 2, 0, 4 },
            { -2, 1, 2 },
            { -1, 1, 4 },
            { 0, 1, 8 },
            { 1, 1, 4 },
            { 2, 1, 2 }
        },
        32
    };

    PixelArtErrorDiffusionKernel sierraKernel = {
        {
            { 1, 0, 5 },
            { 2, 0, 3 },
            { -2, 1, 2 },
            { -1, 1, 4 },
            { 0, 1, 5 },
            { 1, 1, 4 },
            { 2, 1, 2 },
            { -1, 2, 2 },
            { 0, 2, 3 },
            { 1, 2, 2 },
        },
        32
    };

    PixelArtErrorDiffusionKernel sierraTwoRowKernel = {
        {
            { 1, 0, 4 },
            { 2, 0, 3 },
            { -2, 1, 1 },
            { -1, 1, 2 },
            { 0, 1, 3 },
            { 1, 1, 2 },
            { 2, 1, 1 },
        },
        16
    };

    PixelArtErrorDiffusionKernel sierraLiteKernel = {
        {
            { 1, 0, 2 },
            { -1, 1, 1 },
            { 0, 1, 1 },
        },
        4
    };

    PixelArtDitherAlgorithm algorithm = static_cast<PixelArtDitherAlgorithm>(config->getInt("algorithm"));

    switch(algorithm)
    {
    default:
    case PixelArtDitherAlgorithm::None:
        processPixelsNoDither(device, applyRect, config, progressUpdater, palette);
        break;
    case PixelArtDitherAlgorithm::Bayer2x2:
        processPixelsBayer(device, applyRect, config, progressUpdater, palette, bayer2Kernel);
        break;
    case PixelArtDitherAlgorithm::Bayer4x4:
        processPixelsBayer(device, applyRect, config, progressUpdater, palette, bayer4Kernel);
        break;
    case PixelArtDitherAlgorithm::Bayer8x8:
        processPixelsBayer(device, applyRect, config, progressUpdater, palette, bayer8Kernel);
        break;
    case PixelArtDitherAlgorithm::FloydSteinberg:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, floydSteinbergKernel);
        break;
    case PixelArtDitherAlgorithm::JarvisJudiceNinke:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, jarvisJudiceNinkeKernel);
        break;
    case PixelArtDitherAlgorithm::Stucki:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, stuckiKernel);
        break;
    case PixelArtDitherAlgorithm::Atkinson:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, atkinsonKernel);
        break;
    case PixelArtDitherAlgorithm::Burkes:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, burkesKernel);
        break;
    case PixelArtDitherAlgorithm::Sierra:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, sierraKernel);
        break;
    case PixelArtDitherAlgorithm::SierraTwoRow:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, sierraTwoRowKernel);
        break;
    case PixelArtDitherAlgorithm::SierraLite:
        processPixelsErrorDiffusion(device, applyRect, config, progressUpdater, palette, sierraLiteKernel);
        break;
    }
}

QRect KisPixelArtFilter::neededRect(const QRect & rect, const KisFilterConfigurationSP config, int lod) const
{
    if(isErrorDiffusionAlgorithm(static_cast<PixelArtDitherAlgorithm>(config->getInt("algorithm")))) {
        return QRect(0, 0, USHRT_MAX, USHRT_MAX);
    }
    else {
        KisLodTransformScalar t(lod);

        const int pixelWidth = qCeil(t.scale(config ? qMax(1, config->getInt("pixelWidth", 10)) : 10));
        const int pixelHeight = qCeil(t.scale(config ? qMax(1, config->getInt("pixelHeight", 10)) : 10));

        return rect.adjusted(-2 * pixelWidth, -2 * pixelHeight, 2 * pixelWidth, 2 * pixelHeight);
    }
}

QRect KisPixelArtFilter::changedRect(const QRect & rect, const KisFilterConfigurationSP config, int lod) const
{
    return neededRect(rect, config, lod);
}

KisConfigWidget * KisPixelArtFilter::createConfigurationWidget(QWidget * parent, const KisPaintDeviceSP, bool) const
{
    return new KisWdgPixelArt(parent);
}

KisFilterConfigurationSP KisPixelArtFilter::factoryConfiguration() const
{
    KisFilterConfigurationSP config = new KisFilterConfiguration("pixelArt", 1);
    config->setProperty("pixelWidth", 4);
    config->setProperty("pixelHeight", 4);
    config->setProperty("algorithm", static_cast<int>(PixelArtDitherAlgorithm::Bayer8x8));
    config->setProperty("useLab", 1);

    // Default palette - DawnBringer 32
    QVector<QRgb> colors;
    colors.push_back(0x000000);
    colors.push_back(0x222034);
    colors.push_back(0x45283c);
    colors.push_back(0x663931);
    colors.push_back(0x8f563b);
    colors.push_back(0xdf7126);
    colors.push_back(0xd9a066);
    colors.push_back(0xeec39a);
    colors.push_back(0xfbf236);
    colors.push_back(0x99e550);
    colors.push_back(0x6abe30);
    colors.push_back(0x37946e);
    colors.push_back(0x4b692f);
    colors.push_back(0x524b24);
    colors.push_back(0x323c39);
    colors.push_back(0x3f3f74);
    colors.push_back(0x306082);
    colors.push_back(0x5b6ee1);
    colors.push_back(0x639bff);
    colors.push_back(0x5fcde4);
    colors.push_back(0xcbdbfc);
    colors.push_back(0xffffff);
    colors.push_back(0x9badb7);
    colors.push_back(0x847e87);
    colors.push_back(0x696a6a);
    colors.push_back(0x595652);
    colors.push_back(0x76428a);
    colors.push_back(0xac3232);
    colors.push_back(0xd95763);
    colors.push_back(0xd77bba);
    colors.push_back(0x8f974a);
    colors.push_back(0x8a6f30);

    QStringList palette;
    for (QRgb & color : colors) {
        palette.push_back(QString::number(color));
    }

    config->setProperty("palette", palette);
    return config;
}

quint64 KisPixelArtFilter::diffColor(PixelArtLab lab1, PixelArtLab lab2)
{
    auto diffSqrd = [](quint16 a, quint16 b) -> quint64
    {
        quint64 d = static_cast<quint64>(a > b ? a - b : b - a);
        return d * d;
    };

    quint64 dvl = diffSqrd(lab1.laba[0], lab2.laba[0]);
    quint64 dva = diffSqrd(lab1.laba[1], lab2.laba[1]);
    quint64 dvb = diffSqrd(lab1.laba[2], lab2.laba[2]);

    return dvl + dva + dvb;
}

quint64 KisPixelArtFilter::diffColor(PixelArtRGBA rgba1, PixelArtRGBA rgba2)
{
    auto diffSqrd = [](quint16 a, quint16 b) -> quint64
    {
        quint64 d = static_cast<quint64>(a > b ? a - b : b - a);
        return d * d;
    };

    quint64 dvr = diffSqrd(rgba1.rgba[0], rgba2.rgba[0]);
    quint64 dvg = diffSqrd(rgba1.rgba[1], rgba2.rgba[1]);
    quint64 dvb = diffSqrd(rgba1.rgba[2], rgba2.rgba[2]);

    return dvr + dvg + dvb;
}

void KisPixelArtFilter::scaleAppendErrorShift(PixelArtRGBAError & error, const PixelArtRGBAError & append, quint32 scale,
    quint32 shift)
{
    auto scaleShift = [](qint32 v, quint32 scale, quint32 shift) -> qint32
    {
        return v > 0 ? (v * scale) >> shift : -((-v * scale) >> shift);
    };

    error.rgba[0] += scaleShift(append.rgba[0], scale, shift);
    error.rgba[1] += scaleShift(append.rgba[1], scale, shift);
    error.rgba[2] += scaleShift(append.rgba[2], scale, shift);
}

void KisPixelArtFilter::scaleAppendErrorDivide(PixelArtRGBAError & error, const PixelArtRGBAError & append, quint32 scale,
    quint32 denom)
{
    auto scaleDivide = [](qint32 v, qint32 scale, qint32 denom) -> qint32
    {
        return (v * scale) / denom;
    };

    error.rgba[0] += scaleDivide(append.rgba[0], scale, denom);
    error.rgba[1] += scaleDivide(append.rgba[1], scale, denom);
    error.rgba[2] += scaleDivide(append.rgba[2], scale, denom);
}

bool KisPixelArtFilter::isErrorDiffusionAlgorithm(PixelArtDitherAlgorithm algorithm)
{
    switch(algorithm)
    {
    case PixelArtDitherAlgorithm::None:
    case PixelArtDitherAlgorithm::Bayer2x2:
    case PixelArtDitherAlgorithm::Bayer4x4:
    case PixelArtDitherAlgorithm::Bayer8x8:
        return false;
    default:
        return true;
    }
}

KisPixelArtFilter::PaletteFindResult KisPixelArtFilter::findClosestColor(
    const PixelArtRGBA & color,
    bool useLab,
    const KoColorSpace * rgbColorSpace,
    const QVector<PixelArtPaletteEntry> & palette)
{
    quint64 closestDist = ULONG_LONG_MAX;
    PaletteFindResult result = {};

    auto processDist = [&](quint64 dist, PixelArtRGBA rgba)
    {
        if (dist < closestDist) {
            qint32 dr = static_cast<qint32>(color.rgba[0]) - static_cast<qint32>(rgba.rgba[0]);
            qint32 dg = static_cast<qint32>(color.rgba[1]) - static_cast<qint32>(rgba.rgba[1]);
            qint32 db = static_cast<qint32>(color.rgba[2]) - static_cast<qint32>(rgba.rgba[2]);

            closestDist = dist;
            result.color = rgba;
            result.error = PixelArtRGBAError{ dr, dg, db, 0 };
        }
    };

    if(useLab) {
        PixelArtLab colorLab;
        rgbColorSpace->toLabA16(reinterpret_cast<const quint8 *>(&color),
            reinterpret_cast<quint8 *>(&colorLab), 1);

        for (const PixelArtPaletteEntry & paletteEntry : palette) {
            quint64 dist = diffColor(colorLab, paletteEntry.lab);
            processDist(dist, paletteEntry.rgba);
        }
    }
    else {
        for (const PixelArtPaletteEntry & paletteEntry : palette) {
            quint64 dist = diffColor(color, paletteEntry.rgba);
            processDist(dist, paletteEntry.rgba);
        }
    }

    return result;
}

KisPixelArtFilter::PaletteFindTwoClosestResult KisPixelArtFilter::findTwoClosestColors(
    const PixelArtRGBA & color,
    bool useLab,
    const KoColorSpace * rgbColorSpace,
    const QVector<PixelArtPaletteEntry> & palette)
{
    PaletteFindTwoClosestResult result = {};
    result.colorDiff1 = ULONG_LONG_MAX;
    result.colorDiff2 = ULONG_LONG_MAX;

    auto processDist = [&](quint64 dist, PixelArtRGBA rgba, PixelArtLab lab)
    {
        if (dist < result.colorDiff1) {
            result.color2 = result.color1;
            result.colorDiff2 = result.colorDiff1;
            result.lab2 = result.lab1;

            result.color1 = rgba;
            result.colorDiff1 = dist;
            result.lab1 = lab;
        }
        else if(dist < result.colorDiff2)
        {
            result.color2 = rgba;
            result.colorDiff2 = dist;
            result.lab2 = lab;
        }
    };

    if(useLab) {
        PixelArtLab colorLab;
        rgbColorSpace->toLabA16(reinterpret_cast<const quint8 *>(&color),
            reinterpret_cast<quint8 *>(&colorLab), 1);

        for (const PixelArtPaletteEntry & paletteEntry : palette) {
            quint64 dist = diffColor(colorLab, paletteEntry.lab);
            processDist(dist, paletteEntry.rgba, paletteEntry.lab);
        }
    }
    else {
        for (const PixelArtPaletteEntry & paletteEntry : palette) {
            quint64 dist = diffColor(color, paletteEntry.rgba);
            processDist(dist, paletteEntry.rgba, paletteEntry.lab);
        }
    }

    if(result.lab1.laba[0] > result.lab2.laba[0])
    {
        std::swap(result.color1, result.color2);
        std::swap(result.lab1, result.lab2);
        std::swap(result.colorDiff1, result.colorDiff2);
    }

    return result;
}

void KisPixelArtFilter::processPixelsNoDither(
    KisPaintDeviceSP device,
    const QRect & applyRect,
    const KisFilterConfigurationSP config,
    KoUpdater * progressUpdater,
    const QVector<PixelArtPaletteEntry> & palette) const
{
    const KoColorSpace * cs = device->colorSpace();

    KisLodTransformScalar t(device);
    const int pixelWidth = qCeil(t.scale(config ? qMax(1, config->getInt("pixelWidth", 10)) : 10));
    const int pixelHeight = qCeil(t.scale(config ? qMax(1, config->getInt("pixelHeight", 10)) : 10));

    const qint32 pixelSize = device->pixelSize();

    const QRect deviceBounds = device->defaultBounds()->bounds();

    const int bufferSize = pixelSize * pixelWidth * pixelHeight;
    QScopedArrayPointer<quint8> buffer(new quint8[bufferSize]);

    KoColor pixelColor(Qt::black, device->colorSpace());
    KoMixColorsOp *mixOp = device->colorSpace()->mixColorsOp();

    using namespace KisAlgebra2D;
    const qint32 firstCol = divideFloor(applyRect.x(), pixelWidth);
    const qint32 firstRow = divideFloor(applyRect.y(), pixelHeight);

    const qint32 lastCol = divideFloor(applyRect.x() + applyRect.width() - 1, pixelWidth);
    const qint32 lastRow = divideFloor(applyRect.y() + applyRect.height() - 1, pixelHeight);

    bool useLab = config->getInt("useLab") != 0;
    const KoColorSpace * rgbColorSpace = KoColorSpaceRegistry::instance()->rgb16();

    progressUpdater->setRange(firstRow, lastRow);

    for(qint32 i = firstRow; i <= lastRow; i++) {
        for(qint32 j = firstCol; j <= lastCol; j++) {
            const QRect maxPatchRect(j * pixelWidth, i * pixelHeight,
                pixelWidth, pixelHeight);
            const QRect pixelRect = maxPatchRect & deviceBounds;
            const int numColors = pixelRect.width() * pixelRect.height();


            //read
            KisSequentialConstIterator srcIt(device, pixelRect);

            memset(buffer.data(), 0, bufferSize);
            quint8 *bufferPtr = buffer.data();

            while (srcIt.nextPixel()) {
                memcpy(bufferPtr, srcIt.oldRawData(), pixelSize);
                bufferPtr += pixelSize;
            }

            // mix all the colors
            mixOp->mixColors(buffer.data(), numColors, pixelColor.data());

            PixelArtRGBA rgbaColor;
            cs->toRgbA16(reinterpret_cast<const quint8 *>(pixelColor.data()),
                reinterpret_cast<quint8 *>(&rgbaColor), 1);

            PaletteFindResult result = findClosestColor(rgbaColor, useLab, rgbColorSpace, palette);

            cs->fromRgbA16(reinterpret_cast<const quint8 *>(&result.color),
                reinterpret_cast<quint8 *>(pixelColor.data()), 1);

            // write only colors in applyRect
            const QRect writeRect = pixelRect & applyRect;

            KisSequentialIterator dstIt(device, writeRect);
            while (dstIt.nextPixel()) {
                memcpy(dstIt.rawData(), pixelColor.data(), pixelSize);
            }
        }
        progressUpdater->setValue(i);
    }
}

void KisPixelArtFilter::processPixelsBayer(
    KisPaintDeviceSP device,
    const QRect & applyRect,
    const KisFilterConfigurationSP config,
    KoUpdater * progressUpdater,
    const QVector<PixelArtPaletteEntry> & palette,
    const PixelArtBayerKernel & kernel) const
{
    const KoColorSpace * cs = device->colorSpace();

    KisLodTransformScalar t(device);
    const int pixelWidth = qCeil(t.scale(config ? qMax(1, config->getInt("pixelWidth", 10)) : 10));
    const int pixelHeight = qCeil(t.scale(config ? qMax(1, config->getInt("pixelHeight", 10)) : 10));

    const qint32 pixelSize = device->pixelSize();

    const QRect deviceBounds = device->defaultBounds()->bounds();

    const int bufferSize = pixelSize * pixelWidth * pixelHeight;
    QScopedArrayPointer<quint8> buffer(new quint8[bufferSize]);

    KoColor pixelColor(Qt::black, device->colorSpace());
    KoMixColorsOp *mixOp = device->colorSpace()->mixColorsOp();

    using namespace KisAlgebra2D;
    const qint32 firstCol = divideFloor(applyRect.x(), pixelWidth);
    const qint32 firstRow = divideFloor(applyRect.y(), pixelHeight);

    const qint32 lastCol = divideFloor(applyRect.x() + applyRect.width() - 1, pixelWidth);
    const qint32 lastRow = divideFloor(applyRect.y() + applyRect.height() - 1, pixelHeight);

    bool useLab = config->getInt("useLab") != 0;
    const KoColorSpace * rgbColorSpace = KoColorSpaceRegistry::instance()->rgb16();

    progressUpdater->setRange(firstRow, lastRow);

    qint32 matrixMask = kernel.size - 1;
    qint32 matrixSizeSquared = (kernel.size * kernel.size);
    qint32 shift = 0;

    for(int bit = 0; bit < 16; ++bit) {
        if((1 << bit) == kernel.size) {
            shift = bit;
            break;
        }
    }

    assert(shift > 0);

    for(qint32 i = firstRow; i <= lastRow; i++) {
        for(qint32 j = firstCol; j <= lastCol; j++) {
            const QRect maxPatchRect(j * pixelWidth, i * pixelHeight,
                pixelWidth, pixelHeight);
            const QRect pixelRect = maxPatchRect & deviceBounds;
            const int numColors = pixelRect.width() * pixelRect.height();

            //read
            KisSequentialConstIterator srcIt(device, pixelRect);

            memset(buffer.data(), 0, bufferSize);
            quint8 *bufferPtr = buffer.data();

            while (srcIt.nextPixel()) {
                memcpy(bufferPtr, srcIt.oldRawData(), pixelSize);
                bufferPtr += pixelSize;
            }

            // mix all the colors
            mixOp->mixColors(buffer.data(), numColors, pixelColor.data());

            PixelArtRGBA rgbaColor;
            cs->toRgbA16(reinterpret_cast<const quint8 *>(pixelColor.data()),
                reinterpret_cast<quint8 *>(&rgbaColor), 1);

            PaletteFindTwoClosestResult result = findTwoClosestColors(rgbaColor, useLab, rgbColorSpace, palette);

            int matrixX = i & matrixMask;
            int matrixY = j & matrixMask;
            int matrixIndex = (matrixY << shift) + matrixX;
            int matrixValue = kernel.matrix[matrixIndex];
            quint64 testValue = (result.colorDiff1 + result.colorDiff2) ?
                (matrixSizeSquared * result.colorDiff1) / (result.colorDiff1 + result.colorDiff2) : 0;

            PixelArtRGBA finalColor = testValue <= static_cast<quint64>(matrixValue) ? result.color1 : result.color2;

            finalColor.rgba[3] = rgbaColor.rgba[3];
            cs->fromRgbA16(reinterpret_cast<const quint8 *>(&finalColor),
                reinterpret_cast<quint8 *>(pixelColor.data()), 1);

            // write only colors in applyRect
            const QRect writeRect = pixelRect & applyRect;

            KisSequentialIterator dstIt(device, writeRect);
            while (dstIt.nextPixel()) {
                memcpy(dstIt.rawData(), pixelColor.data(), pixelSize);
            }
        }
        progressUpdater->setValue(i);
    }
}

void KisPixelArtFilter::processPixelsErrorDiffusion(
    KisPaintDeviceSP device,
    const QRect & applyRect,
    const KisFilterConfigurationSP config,
    KoUpdater * progressUpdater,
    const QVector<PixelArtPaletteEntry> & palette,
    const PixelArtErrorDiffusionKernel & kernel) const
{
    const KoColorSpace * cs = device->colorSpace();

    KisLodTransformScalar t(device);
    const int pixelWidth = qCeil(t.scale(config ? qMax(1, config->getInt("pixelWidth", 10)) : 10));
    const int pixelHeight = qCeil(t.scale(config ? qMax(1, config->getInt("pixelHeight", 10)) : 10));

    const qint32 pixelSize = device->pixelSize();

    const QRect deviceBounds = device->defaultBounds()->bounds();

    const int mixBufferSize = pixelSize * pixelWidth * pixelHeight;
    QScopedArrayPointer<quint8> mixBuffer(new quint8[mixBufferSize]);

    KoColor pixelColor(Qt::black, cs);
    KoMixColorsOp * mixOp = cs->mixColorsOp();

    using namespace KisAlgebra2D;

    const int pixelBufferWidth = (deviceBounds.width() + pixelWidth - 1) / pixelWidth;
    const int pixelBufferHeight = (deviceBounds.height() + pixelHeight - 1) / pixelHeight;
    const int pixelBufferSize = pixelBufferWidth * pixelBufferHeight;

    QScopedArrayPointer<PixelArtRGBA> pixelBuffer(new PixelArtRGBA[pixelBufferSize]);
    PixelArtRGBA * pixelBufferPtr = pixelBuffer.data();

    for (qint32 y = deviceBounds.top(); y <= deviceBounds.bottom(); y += pixelHeight) {
        for (qint32 x = deviceBounds.left(); x <= deviceBounds.right(); x += pixelWidth) {
            const QRect maxPatchRect(x, y, pixelWidth, pixelHeight);
            const QRect pixelRect = maxPatchRect & deviceBounds;
            const int numColors = pixelRect.width() * pixelRect.height();

            //read
            KisSequentialConstIterator srcIt(device, pixelRect);

            memset(mixBuffer.data(), 0, mixBufferSize);
            quint8 * mixBufferPtr = mixBuffer.data();

            while (srcIt.nextPixel()) {
                memcpy(mixBufferPtr, srcIt.oldRawData(), pixelSize);
                mixBufferPtr += pixelSize;
            }

            // mix all the colors
            mixOp->mixColors(mixBuffer.data(), numColors, pixelColor.data());

            PixelArtRGBA rgba;
            cs->toRgbA16(reinterpret_cast<const quint8 *>(pixelColor.data()), reinterpret_cast<quint8 *>(&rgba), 1);

            *pixelBufferPtr = rgba;

            pixelBufferPtr++;
        }
    }

    QScopedArrayPointer<PixelArtRGBAError> errorBuffer(new PixelArtRGBAError[pixelBufferSize]);
    memset(errorBuffer.data(), 0, sizeof(PixelArtRGBAError) * static_cast<std::size_t>(pixelBufferSize));

    bool useLab = config->getInt("useLab") != 0;
    const KoColorSpace * rgbColorSpace = KoColorSpaceRegistry::instance()->rgb16();

    PixelArtRGBA * pixel = pixelBuffer.data();
    PixelArtRGBAError * error = errorBuffer.data();

    int numKernelElems = kernel.kernelElements.size();
    QVector<PixelArtRGBAError *> kernelPtrs;
    for(auto & elem : kernel.kernelElements) {
        PixelArtRGBAError * ptr = errorBuffer.data() + (elem.y * pixelBufferWidth) + elem.x;
        kernelPtrs.push_back(ptr);
    }

    bool divide = true;
    int shift = 0;

    for(int bit = 0; bit < 16; ++bit) {
        if((1 << bit) == kernel.denom) {
            divide = false;
            shift = bit;
            break;
        }

        if((1 << bit) > kernel.denom) {
            break;
        }
    }

    progressUpdater->setRange(0, pixelBufferHeight);

    for (int y = 0; y < pixelBufferHeight; ++y) {
        for (int x = 0; x < pixelBufferWidth; ++x) {
            auto clampedAdd = [](quint16 a, qint32 b) -> quint16
            {
                qint32 v = static_cast<qint32>(a) + b;
                return v < 0 ? 0 : (v > USHRT_MAX ? USHRT_MAX : v);
            };

            PixelArtRGBA adjustedPixel;
            adjustedPixel.rgba[0] = clampedAdd(pixel->rgba[0], error->rgba[0]);
            adjustedPixel.rgba[1] = clampedAdd(pixel->rgba[1], error->rgba[1]);
            adjustedPixel.rgba[2] = clampedAdd(pixel->rgba[2], error->rgba[2]);
            adjustedPixel.rgba[3] = pixel->rgba[3];

            PaletteFindResult result = findClosestColor(adjustedPixel, useLab, rgbColorSpace, palette);

            *pixel = result.color;
            pixel->rgba[3] = adjustedPixel.rgba[3];

            if(!divide) {
                for (int elemIndex = 0; elemIndex < numKernelElems; ++elemIndex) {
                    auto & ptr = kernelPtrs[elemIndex];
                    auto & elem = kernel.kernelElements[elemIndex];

                    if (x + elem.x < pixelBufferWidth && x + elem.x >= 0 &&
                        y + elem.y < pixelBufferHeight &&
                        y + elem.y >= 0)
                    {
                        scaleAppendErrorShift(*ptr, result.error, elem.scale, shift);
                    }

                    ++ptr;
                }
            }
            else {
                for (int elemIndex = 0; elemIndex < numKernelElems; ++elemIndex) {
                    auto & ptr = kernelPtrs[elemIndex];
                    auto & elem = kernel.kernelElements[elemIndex];

                    if (x + elem.x < pixelBufferWidth && x + elem.x >= 0 &&
                        y + elem.y < pixelBufferHeight &&
                        y + elem.y >= 0)
                    {
                        scaleAppendErrorDivide(*ptr, result.error, elem.scale, kernel.denom);
                    }

                    ++ptr;
                }
            }

            ++pixel;
            ++error;
        }

        progressUpdater->setValue(y);
    }

    const qint32 firstCol = divideFloor(applyRect.x(), pixelWidth);
    const qint32 firstRow = divideFloor(applyRect.y(), pixelHeight);

    const qint32 lastCol = divideFloor(applyRect.x() + applyRect.width() - 1, pixelWidth);
    const qint32 lastRow = divideFloor(applyRect.y() + applyRect.height() - 1, pixelHeight);

    for (qint32 y = firstRow; y <= lastRow; y++) {
        for (qint32 x = firstCol; x <= lastCol; x++) {
            const QRect maxPatchRect(x * pixelWidth, y * pixelHeight,
                pixelWidth, pixelHeight);
            const QRect pixelRect = maxPatchRect & deviceBounds;

            // write only colors in applyRect
            const QRect writeRect = pixelRect & applyRect;

            int pixelOffset = (y * pixelBufferWidth) + x;
            pixelBufferPtr = pixelBuffer.data() + pixelOffset;

            cs->fromRgbA16(reinterpret_cast<const quint8 *>(pixelBufferPtr), pixelColor.data(), 1);

            KisSequentialIterator dstIt(device, writeRect);
            while (dstIt.nextPixel()) {
                memcpy(dstIt.rawData(), reinterpret_cast<const quint8 *>(pixelColor.data()),
                    static_cast<std::size_t>(pixelSize));
            }
        }
    }
}

